﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Dialogue List", menuName = "Create Dialogue List", order = 2)]
public class DialogueList : ScriptableObject
{
    static public List<Dialogue> AllDialogues; 
    static public short IDCount; 
}
