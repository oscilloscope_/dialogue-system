﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.IO;

/*

                                    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
                                    ░░░░░░░░░░▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄░░░░░░░░░
                                    ░░░░░░░░▄▀░░░░░░░░░░░░▄░░░░░░░▀▄░░░░░░░
                                    ░░░░░░░░█░░▄░░░░▄░░░░░░░░░░░░░░█░░░░░░░
                                    ░░░░░░░░█░░░░░░░░░░░░▄█▄▄░░▄░░░█░▄▄▄░░░
                                    ░▄▄▄▄▄░░█░░░░░░▀░░░░▀█░░▀▄░░░░░█▀▀░██░░
                                    ░██▄▀██▄█░░░▄░░░░░░░██░░░░▀▀▀▀▀░░░░██░░
                                    ░░▀██▄▀██░░░░░░░░▀░██▀░░░░░░░░░░░░░▀██░
                                    ░░░░▀████░▀░░░░▄░░░██░░░▄█░░░░▄░▄█░░██░
                                    ░░░░░░░▀█░░░░▄░░░░░██░░░░▄░░░▄░░▄░░░██░
                                    ░░░░░░░▄█▄░░░░░░░░░░░▀▄░░▀▀▀▀▀▀▀▀░░▄▀░░
                                    ░░░░░░█▀▀█████████▀▀▀▀████████████▀░░░░
                                    ░░░░░░████▀░░███▀░░░░░░▀███░░▀██▀░░░░░░
                                    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
                                    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
                                    ░░░░░░░AUTHOR  :  Saika Fatih░░░░░░░░░░
                                    ░░░░░░░░░░░░░░25.09.2017░░░░░░░░░░░░░░░
                                    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░

 * 
 * MVC Dialogue System (Serilizable) 
 * Diyalog sistemindeki serilize edilecek jenerik sözlük ve listeler DialogueController scriptinde hazırlanıyor.
 * Buradan MVC patternine ilişkin geliştirilen sınıf ve fonksiyonlar instance edilip , kullanılıyor. 
 * DialogueView scriptinden arayüz ile ilişkisi yönetiliyor.
 * Diyaloglar SO(scriptable object)lar halinde oluşturulmuş halde Editorde mevcut bulunuyor.Bunlar buradaki
 * sözlük ve listeler ile serilize edilecek şekilde JsonController kısmında ilişkisel hale getirilip
 * "DialogueContext.json" uzantısı ile serilize dosyasına yazılıyor.Bu dosya manuel yada otomatik olarak
 * kullanıcının telefonunda (iOS veya Android) oluşturulacak.Oyun boyunca oyuncunun takip edeceği
 * görev yolundan bağımsız olarak yine görevlerle ilişkili biçimde buradan okunacak veriler ile diyaloglar
 * kontrol edilecek.

 
<-Yapılacaklar (TODO) listesi / Yapılanlar Listesi->---------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
- ->  [+] Serilize edilecek listeler ve sözlükler hazırlandı                                                      -
- ->  [+] Json dosyasına kaydedilerek bu listeler ve sözlükler anlamlı biçimde NPC ve harita temelinde yazıldı    -
- ->  [+] Bu serilizasyon dosyası gerçek zamanlı okunabildi ve esnek olarak görevlerle ilişkili çalışabildi.      -
-------------------------------------------------------------------------------------------------------------------                                                                                                                -
-------------------------------------------------------------------------------------------------------------------

    Örnek ilişki: 
    A diyaloğu > B diyaloğu > D diyaloğu   -------- Bid - Aid < id , parentID >  -> Did - Bid <id , parentID> 
    
    Genel olarak görevlerle birlikte ilişki:
    
    d1  , d2 , d3 , d4 , ...... , dn ( diyaloglar )


    d1 = Root 

    d1 <- d2 -(+)- d3 -(+)- d5  [ Burada d1 ana diyalog , d1 diyaloğundan sonra d2 diyaloğu var. d2 diyaloğu da bir 
          |	                      şartlı diyaloğun -(+) şart olarak temsil edildi- birde normal akış diyaloğunun ebeveyni. ]
          d4 -(+)- d6
          |
          d7	

*/

// Tüm diyaloglar Json.LDialogueTextsPassList listesinde listeleniyor.


namespace DialogueSys
{
    public class DialogueSystem : MonoBehaviour
    {
        private DialogueController mDialogueController;
        private DialogueView mDialogueView;
        private Json mJsonNPCController; 

        public UnityEngine.UI.Text DialogueText;
        public bool AllowNext;
         
        public UnityEngine.UI.Text DebugLogText;
        Quest.EQuestGiver NPC;

        void Awake()
        {
            Json.SAndroidPath = Path.Combine(Application.persistentDataPath, "DialogueContext.json"); 
        }

        void Start()
        { 
            /* 
                If DialogueContext file is not exist  , download file from NET,
                or if file is exist, just go on
            */
              
            DebugLogText.text = "Yazıldı : " + Application.persistentDataPath.ToString();
            DebugLogText.text += "----" +  Json.SAndroidPath.ToString();
            // TEST
            //mDialogueController = DialogueController.Instance;
            //mDialogueController.MakeRelationTree(Dialogue.LDialogues);

            if (File.Exists(Json.SAndroidPath))
            {
                mJsonNPCController = Json.Instance;

                mDialogueView = DialogueView.Instance;
                mDialogueView.InitView();
                AllowNext = true;
                NPCTest.NPCName = "npc0";

                int k = 0;
                foreach (var dialogue in Json.LDialogueTextsPassList)
                {
                    if (k == 0)
                    {
                        Debug.Log("Root dialogue =>" + dialogue.ToString());
                    }
                    else
                    {
                        Debug.Log("Child dialogue => " + dialogue.ToString());
                    }
                    k++;
                }
            }

            else
            {
                DebugLogText.text += "------" + "Dosya Mevcut Değil!";
            }
        }

        int i = 0;
        void Update()
        {
            if(Input.GetMouseButtonDown(0))
            {
                NextDialogue();
            }

            if(AllowNext)
            {
                string npc_name = NPCTest.NPCName;
                Debug.Log(npc_name);
                switch (npc_name)
                {
                    case "npc0":
                        NPC = Quest.EQuestGiver.NPC0;
                        Debug.Log("NPC0");
                        break;
                    case "npc1":
                        NPC = Quest.EQuestGiver.NPC1;
                        Debug.Log("NPC1");
                        break;
                    case "npc2":
                        NPC = Quest.EQuestGiver.NPC2;
                        Debug.Log("NPC2");
                        break;
                    case "npc3":
                        NPC = Quest.EQuestGiver.System;
                        Debug.Log("NPC3");
                        break;
                    default:
                        Debug.Log("NPC işaretlenemedi");
                        break;
                }

                mJsonNPCController.GetDialogueOfNPCWithID(mJsonNPCController.GetRootDialogueOfNPC(NPC).child_dialogues);
            }

            if (Json.LDialogueTextsPassList != null)
            {
                if (AllowNext)
                {

                    if (i == Json.LDialogueTextsPassList.Capacity)
                    {

                    }
                    i++;

                    if (mJsonNPCController.SetDialogueOfNPCWithID(NPC, 1, true))
                    {
                        Debug.Log(NPC.ToString() + " için tekrardan başarıyla yazıldı");
                    }

                    //if(SetDialogueOfNPC(Quest.EQuestGiver.NPC1, true))
                    //{
                    //    Debug.Log(Quest.EQuestGiver.NPC1.ToString() + " için tekrardan başarıyla yazıldı");
                    //}
                }

                try
                {
                    if (i < Json.LDialogueTextsPassList.ToArray().Length)
                    {
                        mDialogueView.UpdateView(DialogueText, Json.LDialogueTextsPassList.ToArray()[i]);
                    }
                    else
                    {
                        i = 0;
                    }
                }
                catch (Exception e)
                {
                    Debug.LogWarning("Diyalog güncellemesiyle ilgili out of range oluyordu tamponlandı!");
                    i = 0;
                }
                AllowNext = false;
            }
        }

        public void NextDialogue()
        {
            AllowNext = true;
            Quest.QuestSystem.StateTest = true;
        }
    }
}



	
