﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/*
 * Bu script diyalogların arayüz ile ilişkisini yönetecek. Belli ilişkiye sahip
 * olan diyalogları json dosyasından parse ettikten sonra oyun lojiğine göre 
 * -NPC lere veya diyaloga girecek oyun objelerine göre- diyalogların akışı yönetilecek.
 * 
 */


public class DialogueView
{ 
    static private DialogueView mInstance; 

    static public List<string> In_LineDialogues;

    static public DialogueView Instance
    {
        get
        {
            if(General.Utility.IfItIsNullCreate<DialogueView>(ref mInstance))
            {
                if(General.Utility.IsNull<List<SerilizationObject>>(Json.MainSerilizableObject))
                {
                    General.Utility.CreateObjectSerially<List<SerilizationObject>>(ref Json.MainSerilizableObject);
                }

                return mInstance;
            }
            else
            {
                return null;
            }
        }
    }

    public void InitView()
    {  
        if (General.Utility.IsNull<List<string>>(In_LineDialogues))
        {
            General.Utility.CreateObjectSerially<List<string>>(ref In_LineDialogues);
        }
         

        //foreach(var dialogueText in DialogueSys.DialogueController.DDialogueRelational)
        //{
        //    if (Go)
        //    {
        //        foreach (var dialogueObject in Dialogue.LDialogues)
        //        {
        //            if (dialogueObject.DialogueID == dialogueText.Key)
        //            {
        //                DialogueTextRedirector.text = dialogueObject.DialogueText;
        //            }
        //        }
        //        Go = false;
        //    }
        //}
        
    } 

    public void UpdateDialogueList(string root , string[] childDialouges)
    {
        In_LineDialogues.Clear();

        In_LineDialogues.Add(root);
        for (int i = 0; i < childDialouges.Length; i++)
        {
            In_LineDialogues.Add(childDialouges[i]);
        } 

    }

    //TODO : Bu fonksiyonun sistem için genişlemeye ihtiyacı olacak
    public void UpdateView(Text TextRef , string text)
    {  
        TextRef.text = text;        
    }
}
