﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using System;

/*
 *                                  
                                    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
                                    ░░░░░░░░░░▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄░░░░░░░░░
                                    ░░░░░░░░▄▀░░░░░░░░░░░░▄░░░░░░░▀▄░░░░░░░
                                    ░░░░░░░░█░░▄░░░░▄░░░░░░░░░░░░░░█░░░░░░░
                                    ░░░░░░░░█░░░░░░░░░░░░▄█▄▄░░▄░░░█░▄▄▄░░░
                                    ░▄▄▄▄▄░░█░░░░░░▀░░░░▀█░░▀▄░░░░░█▀▀░██░░
                                    ░██▄▀██▄█░░░▄░░░░░░░██░░░░▀▀▀▀▀░░░░██░░
                                    ░░▀██▄▀██░░░░░░░░▀░██▀░░░░░░░░░░░░░▀██░
                                    ░░░░▀████░▀░░░░▄░░░██░░░▄█░░░░▄░▄█░░██░
                                    ░░░░░░░▀█░░░░▄░░░░░██░░░░▄░░░▄░░▄░░░██░
                                    ░░░░░░░▄█▄░░░░░░░░░░░▀▄░░▀▀▀▀▀▀▀▀░░▄▀░░
                                    ░░░░░░█▀▀█████████▀▀▀▀████████████▀░░░░
                                    ░░░░░░████▀░░███▀░░░░░░▀███░░▀██▀░░░░░░
                                    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
                                    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
                                    ░░░░░░░AUTHOR  :  Saika Fatih░░░░░░░░░░
                                    ░░░░░░░░░░░░░░25.09.2017░░░░░░░░░░░░░░░
                                    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * 
    Burada Unity Editörde diyaloglar serilize edilmek için yazma işlemine tabi tutuluyor ve bir dosya çıkartılıyor
    Aynı zamanda bu serilize edilen dosyayı okuma işlemleri parent-child ilişkiselliğinde yapılabilmek için
    hem editor için hemde mobil cihazlara uygun fonksiyonlar ayrıldılar.
    
  ->Fonksiyonlar  (PARAMETRELERİ VAR)
    Json.CreateJsonFile();
    Json.CreateJsonFileMobile();
    Json.ReadJsonFileForSetting();
    Json.ReadJsonFileForSettingID();
    Json.ReadJsonFile();
    Json.ReadJsonFileMobile();

  ->Değişkenler
    Json.SAndroidPath;

  ->Listeler
    Json.MainSerilizableObject(); 
    Json.LDialogueTextsPassList();

*/

public partial class Json
{
    private static JsonController mJsonController;
    private static List<string> mDialoguesJson;
    static public List<SerilizationObject> MainSerilizableObject;
    public static string SAndroidPath;
 
    static private Json instance;
    static public string MainStatusOfDialogueSystem;
    static public List<string> LDialogueTextsPassList;
    private SerilizationObject mDialogueSerilizableObject;


    static List<string> dialogue_texts;
#if UNITY_EDITOR
    [MenuItem("Dialogue/Create Context File (JSon)")]
    public static void CreateJsonFile()
    {
        if (General.Utility.IsNull<JsonController>(mJsonController))
        {
            General.Utility.CreateObjectSerially<JsonController>(ref mJsonController);
        }

        if(General.Utility.IsNull<List<string>>(mDialoguesJson))
        {
            General.Utility.CreateObjectSerially<List<string>>(ref mDialoguesJson);
        } 
        //To Directly Json Object
        //List<SerilizationObject> jsonObject = JsonConvert.SerializeObject(mJsonController.WriteFile(DialogueSys.DialogueController.DDialogueRelational));
        DialogueSys.DialogueController.Instance.MakeRelationTree(DialogueList.AllDialogues);
        mDialoguesJson.Add(JsonConvert.SerializeObject(mJsonController.WriteFile(DialogueSys.DialogueController.DDialogueRelational), Formatting.Indented));
         
        foreach (var jout in mDialoguesJson )
        {
            try
            {
                using (StreamWriter file = new StreamWriter(Application.dataPath+"/Systems/DialogueSystem/EditorOutput/DialogueContext.json"))
                {
                    file.Write(jout.ToString());
                    file.Close();
                }
            }
            catch(Exception e)
            {
                Debug.Log("Editor zamanlı bir hata -> DialogueContext bulunamadı : " + e.ToString());
            }
        }
         
    }

    public static void CreateJsonFile(string fileName, List<SerilizationObject> sos)
    {
        if (General.Utility.IsNull<JsonController>(mJsonController))
        {
            General.Utility.CreateObjectSerially<JsonController>(ref mJsonController);
        }

        if (General.Utility.IsNull<List<string>>(mDialoguesJson))
        {
            General.Utility.CreateObjectSerially<List<string>>(ref mDialoguesJson);
        }

        //To Directly Json Object
        //List<SerilizationObject> jsonObject = JsonConvert.SerializeObject(mJsonController.WriteFile(DialogueSys.DialogueController.DDialogueRelational)); 
        mDialoguesJson.Add(JsonConvert.SerializeObject(sos, Formatting.Indented)); 

        foreach (var jout in mDialoguesJson)
        {
            try
            {
                if (fileName != "")
                {
                    using (StreamWriter file = new StreamWriter(SAndroidPath + fileName.ToString() + ".json"))
                    {
                        file.Write(jout.ToString());
                        file.Close();
                    }
                }
                else
                {
                    using (StreamWriter file = new StreamWriter(SAndroidPath))
                    {
                        file.Write(jout.ToString());
                        file.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log("Editor zamanlı bir hata -> DialogueContext bulunamadı : " + e.ToString());
            }
        }

    }
#elif UNITY_ANDROID || UNITY_IOS
    public static void CreateJsonFile()
    {
        if (General.Utility.IsNull<JsonController>(mJsonController))
        {
            General.Utility.CreateObjectSerially<JsonController>(ref mJsonController);
        }

        if (General.Utility.IsNull<List<string>>(mDialoguesJson))
        {
            General.Utility.CreateObjectSerially<List<string>>(ref mDialoguesJson);
        }

        //To Directly Json Object
        //List<SerilizationObject> jsonObject = JsonConvert.SerializeObject(mJsonController.WriteFile(DialogueSys.DialogueController.DDialogueRelational));

        mDialoguesJson.Add(JsonConvert.SerializeObject(mJsonController.WriteFile(DialogueSys.DialogueController.DDialogueRelational), Formatting.Indented));


        foreach (var jout in mDialoguesJson)
        {
            try
            {
                using (StreamWriter file = new StreamWriter(SAndroidPath))
                {
                    file.Write(jout.ToString());
                }
            }
            catch (Exception e)
            {
                Debug.Log("Editor zamanlı bir hata -> DialogueContext bulunamadı : " + e.ToString());
            }
        }

    }

    public static void CreateJsonFile(string fileName, List<SerilizationObject> sos)
    {
        if (General.Utility.IsNull<JsonController>(mJsonController))
        {
            General.Utility.CreateObjectSerially<JsonController>(ref mJsonController);
        }

        if (General.Utility.IsNull<List<string>>(mDialoguesJson))
        {
            General.Utility.CreateObjectSerially<List<string>>(ref mDialoguesJson);
        }

        //To Directly Json Object
        //List<SerilizationObject> jsonObject = JsonConvert.SerializeObject(mJsonController.WriteFile(DialogueSys.DialogueController.DDialogueRelational));


        mDialoguesJson.Add(JsonConvert.SerializeObject(sos, Formatting.Indented));



        foreach (var jout in mDialoguesJson)
        {
            try
            {
                if (fileName != "")
                {
                    using (StreamWriter file = new StreamWriter(SAndroidPath + fileName.ToString() + ".json"))
                    {
                        file.Write(jout.ToString());
                    }
                }
                else
                {
                    using (StreamWriter file = new StreamWriter(SAndroidPath))
                    {
                        file.Write(jout.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log("Editor zamanlı bir hata -> DialogueContext bulunamadı : " + e.ToString());
            }
        }

    }
#endif

}


public partial class Json
{

    static List<string> dialogues_text;
 
    static public void ReadJsonFile()
    {
        try
        {
            using (StreamReader r = new StreamReader(SAndroidPath))
            {
                string json = r.ReadToEnd();
                //List<SerilizationObject> items = JsonConvert.DeserializeObject<List<SerilizationObject>>(json);
                MainSerilizableObject = JsonConvert.DeserializeObject<List<SerilizationObject>>(json);
                foreach (var item in MainSerilizableObject)
                {
                    Debug.Log("dialogue text" + item.dialogue_text + "--- " +
                              "dialogue id " + item.dialogue_id + " --- " +
                              "dialogue map " + item.map + " --- " +
                              "dialogue npc " + item.npc + " --- " +
                              "dialogue parent id = " + item.parent_id + " --- " +
                              "dialogue achieved " + item.quest_achieved + " --- " +
                              "dialogue child ids " + item.child_dialogues.ToString());
                }
                r.Close();
            }
        }
        catch (Exception e)
        {
            Debug.Log("Dosya yolu bulunamadı (DialogueContext Path) => " + e.ToString());
        }
    }

    static public SerilizationObject ReadJsonFileForSetting(string NPC_Data)
    {
        try
        {
            using (StreamReader r = new StreamReader(SAndroidPath))
            {
                string json = r.ReadToEnd();

                if (General.Utility.IsNull<List<SerilizationObject>>(MainSerilizableObject))
                {
                    General.Utility.CreateObjectSerially<List<SerilizationObject>>(ref MainSerilizableObject);
                }

                MainSerilizableObject = JsonConvert.DeserializeObject<List<SerilizationObject>>(json); 
                Debug.Log("MainSerilizableObject length => " + MainSerilizableObject.Capacity.ToString());

                try
                {
                    foreach (var item in MainSerilizableObject)
                    {
                        if (item.npc.ToString() == NPC_Data)
                        {
                            Debug.Log("dialogue text" + item.dialogue_text + "--- " +
                                      "dialogue id " + item.dialogue_id + " --- " +
                                      "dialogue map " + item.map + " --- " +
                                      "dialogue npc " + item.npc + " --- " +
                                      "dialogue parent id = " + item.parent_id + " --- " +
                                      "dialogue achieved " + item.quest_achieved + " --- " +
                                      "dialogue child ids " + item.child_dialogues.ToString());
                            return item;
                        } 
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(e + " --- Yazma Hatası");
                }
                r.Close();
            }
            return new SerilizationObject(); // ERROR _ FROM _ PASSED
        }
        catch (Exception e)
        {
            Debug.Log("Dosya yolu bulanamadı(Dialogue) => " + e.ToString());
            return new SerilizationObject(); // ERROR _ FROM _ PASSED
        }
    }

    static public SerilizationObject ReadJsonFileForSettingID(string NPC_Data,short ID)
    {
        try
        {
            using (StreamReader r = new StreamReader(SAndroidPath))
            {
                string json = r.ReadToEnd();

                if (General.Utility.IsNull<List<SerilizationObject>>(MainSerilizableObject))
                {
                    General.Utility.CreateObjectSerially<List<SerilizationObject>>(ref MainSerilizableObject);
                }

                MainSerilizableObject = JsonConvert.DeserializeObject<List<SerilizationObject>>(json);


                Debug.Log("MainSerilizableObject length => " + MainSerilizableObject.Capacity.ToString());

                try
                {
                    foreach (var item in MainSerilizableObject)
                    {
                        if (item.npc.ToString() == NPC_Data)
                        {
                            if (item.dialogue_id == ID)
                            {
                                Debug.Log("dialogue text" + item.dialogue_text + "--- " +
                                          "dialogue id " + item.dialogue_id + " --- " +
                                          "dialogue map " + item.map + " --- " +
                                          "dialogue npc " + item.npc + " --- " +
                                          "dialogue parent id = " + item.parent_id + " --- " +
                                          "dialogue achieved " + item.quest_achieved + " --- " +
                                          "dialogue child ids " + item.child_dialogues.ToString());
                                return item;
                            }
                        } 
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(e + " --- Yazma Hatası");
                }
            }
            return new SerilizationObject(); // ERROR _ FROM _ PASSED
        }
        catch (Exception e)
        {
            Debug.Log("Dosya yolu bulanamadı(Dialogue) => " + e.ToString());
            return new SerilizationObject(); // ERROR _ FROM _ PASSED
        }
    }
    
    static public List<string> ReadJsonFile(string NPC_Data)
    { 
        if (General.Utility.IsNull<List<string>>(dialogues_text))
        {
            General.Utility.CreateObjectSerially<List<string>>(ref dialogues_text);
        }
        dialogues_text.Clear();

        try
        {
            Debug.Log("Path => " + SAndroidPath.ToString());
            using (StreamReader r = new StreamReader(SAndroidPath))
            {
                string json = r.ReadToEnd();
                MainSerilizableObject = JsonConvert.DeserializeObject<List<SerilizationObject>>(json);

                try
                {
                    foreach (var item in MainSerilizableObject)
                    {
                        if (item.npc.ToString() == NPC_Data)
                        {
                            dialogues_text.Add(item.dialogue_text);
                            Debug.Log(item.dialogue_id.ToString() + " idli " + item.dialogue_text.ToString());
                            //for (in1 t i = 0; i < item.child_dialogues.Length; i++)
                            //{
                            //    foreach (var item_specific in MainSerilizableObject)
                            //    {
                            //        if (item.child_dialogues[i] == item_specific.dialogue_id)
                            //        {
                            //            dialogues_text.Add(item_specific.dialogue_text);
                            //            string child_ids = "";

                            //            for (int t= 0;  t< item.child_dialogues.Length; t++)
                            //            {
                            //                child_ids += item.child_dialogues[t].ToString() + " -- ";
                            //            }
                            //            Debug.Log("Child Dialogues " + item_specific.dialogue_text.ToString()); 
                            //            Debug.Log("dialogue text" + item.dialogue_text + "--- " +
                            //                      "dialogue id " + item.dialogue_id + " --- " +
                            //                      "dialogue map " + item.map + " --- " +
                            //                      "dialogue npc " + item.npc + " --- " +
                            //                      "dialogue parent id = " + item.parent_id + " --- " +
                            //                      "dialogue achieved " + item.quest_achieved + " --- " +
                            //                      "dialogue child ids " + child_ids);
                            //        }
                            //    }
                            //}
                        } 
                        else
                        {
                            Debug.Log("İçine giremedi");
                        }
                        r.Close();
                    }
                    return dialogues_text;
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e + " --  Dosya okuma hatası ");
                    return null;
                }
            }
        }
        catch(Exception e)
        {
            Debug.Log("Dosya yolu bulunamadı (Dialogue) => " + e.ToString());
            return null;
        }
    }
     
}

// İlk dosyayı çalışma zamanında okuma ve üzerine yazma işlemleri buradan yapılıyor.
partial class Json
{ 
    static public Json Instance
    {
        get
        {
            if(instance == null)
            {
                if (General.Utility.IsNull<List<string>>(dialogue_texts))
                {
                    General.Utility.CreateObjectSerially<List<string>>(ref dialogue_texts);
                }

                if (General.Utility.IsNull<List<string>>(LDialogueTextsPassList))
                {
                    General.Utility.CreateObjectSerially<List<string>>(ref LDialogueTextsPassList);
                }
                instance = new Json();
            }

            return instance;
        }
    } 
    public void GetDialogueOfNPC(Quest.EQuestGiver NPC, bool purpose)
    {
        string _NPC_Pass = NPC.ToString();

        LDialogueTextsPassList.Clear();
        dialogue_texts = ReadJsonFile(_NPC_Pass);

        if (purpose)
        {
            try
            {
                foreach (var dialogue_text in dialogue_texts)
                {
                    Debug.LogWarning("Child Dialogue = > " + dialogue_text);
                    LDialogueTextsPassList.Add(dialogue_text);
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning(e + " --- Okuma Hatası");
            }
        }
    }

    public SerilizationObject GetRootDialogueOfNPC(Quest.EQuestGiver NPC)
    {
        string _NPC_Pass = NPC.ToString();

        GetDialogueOfNPC(NPC, false);

        LDialogueTextsPassList.Clear();

        try
        {
            foreach (var is_root in MainSerilizableObject)
            {
                if (is_root.npc.ToString() == _NPC_Pass)
                {
                    for (int i = 0; i < is_root.child_dialogues.Length; i++)
                    {
                        Debug.LogWarning(is_root.child_dialogues[i].ToString() + " -- Boş serilize objesi döndürüldü");
                    }
                    if (is_root.parent_id == -1)
                    {
                        Debug.LogWarning("Root is => " + is_root.ToString());
                        LDialogueTextsPassList.Add(is_root.dialogue_text); // First Dialogue is adding to first;
                        mDialogueSerilizableObject = is_root;
                        Debug.LogWarning(mDialogueSerilizableObject.ToString());
                    }
                    else
                    {
                        Debug.LogWarning("Child is => " + is_root.ToString());
                        LDialogueTextsPassList.Add(is_root.dialogue_text); // First Dialogue is adding to first;
                        mDialogueSerilizableObject = is_root;
                        Debug.LogWarning(mDialogueSerilizableObject.ToString());
                    }
                }
            }
            LDialogueTextsPassList.Reverse();
            return mDialogueSerilizableObject;
        }
        catch(Exception e)
        {
            Debug.LogWarning("Kök serilizasyon objesi alınamadı => " + e.ToString());
            return mDialogueSerilizableObject;
        }
    }

    public void GetDialogueOfNPCWithID(short[] id)
    {
        try
        {
            foreach (var so in MainSerilizableObject)
            {
                if (id != null)
                {
                    for (int i = 0; i < id.Length; i++)
                    {
                        Debug.Log("ID =>" + id[i].ToString());
                        if (so.dialogue_id == id[i])
                        {
                            Debug.LogWarning("ID inside => " + so.dialogue_id.ToString());
                            LDialogueTextsPassList.Add(so.dialogue_text);
                        }
                    }
                }
                else
                {
                    Debug.Log("Dialogue Child List is returned to NULL value");
                }
            }
        }
        catch(Exception e)
        {
            Debug.LogWarning("NPC'nin diyaloğunu id ile alınamadı => " + e.ToString());
        }
    }

    public bool SetDialogueOfNPC(Quest.EQuestGiver NPC, bool isAchieved)
    {
        string _NPC_Pass = NPC.ToString();

        mDialogueSerilizableObject = ReadJsonFileForSetting(_NPC_Pass);
        if (mDialogueSerilizableObject.dialogue_text != "")
        {
            MainSerilizableObject.Remove(mDialogueSerilizableObject);
            mDialogueSerilizableObject.quest_achieved = isAchieved;
            MainSerilizableObject.Add(mDialogueSerilizableObject);
            CreateJsonFile("", MainSerilizableObject); // "" -> for overwrite
            return true;
        }
        return false;
    }

    public bool SetDialogueOfNPCWithID(Quest.EQuestGiver NPC, short ID, bool isAchieved)
    {
        string _NPC_Pass = NPC.ToString();

        mDialogueSerilizableObject = ReadJsonFileForSettingID(_NPC_Pass, ID);
        if (mDialogueSerilizableObject.dialogue_text != "")
        {
            MainSerilizableObject.Remove(mDialogueSerilizableObject);

            mDialogueSerilizableObject.quest_achieved = isAchieved;
            MainSerilizableObject.Add(mDialogueSerilizableObject);
            CreateJsonFile("", MainSerilizableObject); // "" -> for overwrite
            return true;
        }
        return false;
    }

}
