﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCTest : MonoBehaviour
{ 
    private Ray ray;
    private RaycastHit hit;
    static public string NPCName;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        NPC_Selection(ray);
	}

    void NPC_Selection(Ray ray)
    {
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag == "NPC")
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Debug.Log(hit.collider.name.ToString() + " tıklandı");
                    NPCName = hit.collider.name;
                }
            } 
        } 
    }
}
