
                                    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
                                    ░░░░░░░░░░▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄░░░░░░░░░
                                    ░░░░░░░░▄▀░░░░░░░░░░░░▄░░░░░░░▀▄░░░░░░░
                                    ░░░░░░░░█░░▄░░░░▄░░░░░░░░░░░░░░█░░░░░░░
                                    ░░░░░░░░█░░░░░░░░░░░░▄█▄▄░░▄░░░█░▄▄▄░░░
                                    ░▄▄▄▄▄░░█░░░░░░▀░░░░▀█░░▀▄░░░░░█▀▀░██░░
                                    ░██▄▀██▄█░░░▄░░░░░░░██░░░░▀▀▀▀▀░░░░██░░
                                    ░░▀██▄▀██░░░░░░░░▀░██▀░░░░░░░░░░░░░▀██░
                                    ░░░░▀████░▀░░░░▄░░░██░░░▄█░░░░▄░▄█░░██░
                                    ░░░░░░░▀█░░░░▄░░░░░██░░░░▄░░░▄░░▄░░░██░
                                    ░░░░░░░▄█▄░░░░░░░░░░░▀▄░░▀▀▀▀▀▀▀▀░░▄▀░░
                                    ░░░░░░█▀▀█████████▀▀▀▀████████████▀░░░░
                                    ░░░░░░████▀░░███▀░░░░░░▀███░░▀██▀░░░░░░
                                    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
                                    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
                                    ░░░░░░░AUTHOR  :  Saika Fatih░░░░░░░░░░
                                    ░░░░░░░░░░░░░░25.09.2017░░░░░░░░░░░░░░░
                                    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░

 * 
 * MVC Dialogue System (Serilizable) 
 * Diyalog sistemindeki serilize edilecek jenerik sözlük ve listeler DialogueController scriptinde hazırlanıyor.
 * Buradan MVC patternine ilişkin geliştirilen sınıf ve fonksiyonlar yönetiliyor. 
 * DialogueView scriptinden arayüz ile ilişkisi yönetiliyor.
 * Diyaloglar SO(scriptable object)lar halinde oluşturulmuş halde Editorde mevcut bulunuyor.Bunlar buradaki
 * sözlük ve listeler ile serilize edilecek şekilde JsonController kısmında ilişkisel hale getirilip
 * "JsonContext.json" uzantısı ile serilize dosyasına yazılıyor.Bu dosya manuel yada otomatik olarak
 * kullanıcının telefonunda (iOS veya Android) oluşturulacak.Oyun boyunca oyuncunun takip edeceği
 * görev yolundan bağımsız olarak yine görevlerle ilişkili biçimde buradan okunacak veriler ile diyaloglar
 * kontrol edilecek.

 
<-Yapılacaklar (TODO) listesi / Yapılanlar Listesi->---------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
- ->  [+] Serilize edilecek listeler ve sözlükler hazırlandı                                                      -
- ->  [+] Json dosyasına kaydedilecek bu listeler ve sözlükler anlamlı biçimde NPC ve harita temelinde yazıldı    -
- ->  [.] Bu serilizasyon dosyası gerçek zamanlı okunabildi ve esnek olarak görevlerle ilişkili çalışabildi.      -
-------------------------------------------------------------------------------------------------------------------                                                                                                                -
-------------------------------------------------------------------------------------------------------------------

    Örnek ilişki: 
    A diyaloğu > B diyaloğu > D diyaloğu   -------- Bid - Aid < id , parentID >  -> Did - Bid <id , parentID> 
    
    Genel olarak görevlerle birlikte ilişki:
    
    d1  , d2 , d3 , d4 , ...... , dn ( diyaloglar )


    d1 = Root 

    d1 <- d2 -(+)- d3 -(+)- d5  [ Burada d1 ana diyalog , d1 diyaloğundan sonra d2 diyaloğu var. d2 diyaloğu da bir 
          |	                      şartlı diyaloğun -(+) şart olarak temsil edildi- birde normal akış diyaloğunun ebeveyni. ]
          d4 -(+)- d6
          |
          d7	

 