﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class FindDialogues : MonoBehaviour
{

#if UNITY_EDITOR
    static public List<string> Dialogues;

    [MenuItem("Dialogue/Find All Dialogues")]
    static void FindQuestFromAssetManager()
    {
        if (Dialogues == null)
        {
            Dialogues = new List<string>();
        }

        if (DialogueList.AllDialogues == null)
        {
            DialogueList.AllDialogues = new List<Dialogue>();
        }

        string[] _guids;
        _guids = AssetDatabase.FindAssets("l:DialogueObject");
        foreach (var item in _guids)
        {
            string _path = AssetDatabase.GUIDToAssetPath(item);

            if (!Dialogues.Contains(_path))
            {
                Dialogues.Add(_path);
                Debug.Log("Adding Dialogue Object from this path => " + _path);

                if (DialogueList.AllDialogues != null)
                {
                    var _dialogueObject = AssetDatabase.LoadAssetAtPath<Dialogue>(_path);
                    if (DialogueList.AllDialogues.Contains(_dialogueObject))
                    {
                        continue;
                    }
                    else
                    {
                        try
                        {
                            DialogueList.AllDialogues.Add(_dialogueObject);
                        }
                        catch (Exception e)
                        {
                            Debug.LogError("Exception catched : " + e.ToString());
                        }
                    }
                }
            }
            else
            {
                Debug.Log("Already exist in list => " + _path);
                continue;
            }
        }

        foreach (var dialogue in DialogueList.AllDialogues)
        {
            Debug.Log("Dialogue ID => " + dialogue.DialogueID.ToString());
        }
        DialogueList.IDCount = (short)DialogueList.AllDialogues.Capacity;
        
    }
#endif
}

