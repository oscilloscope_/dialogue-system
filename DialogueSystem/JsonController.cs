﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Serialization;
using General;

public class JsonController
{
    List<SerilizationObject> jsonObject;
    public List<SerilizationObject> WriteFile(Dictionary<short, short> pRelationTree)
    {
        if (Utility.IsNull<List<SerilizationObject>>(jsonObject))
        {
            Utility.CreateObjectSerially<List<SerilizationObject>>(ref jsonObject);
        }
         
        SerilizationObject serilizableObject = new SerilizationObject();
        foreach (var retJson in pRelationTree)
        {
            int index = retJson.Key;
            Dialogue[] tempArr = DialogueList.AllDialogues.ToArray();
            serilizableObject.parent_id = tempArr[index].ParentID;
            serilizableObject.quest_achieved = tempArr[index].Achieved;
            serilizableObject.npc = tempArr[index].QuestGiver;
            serilizableObject.map = tempArr[index].MapType;
            serilizableObject.dialogue_text = tempArr[index].DialogueText;
            serilizableObject.child_dialogues = tempArr[index].ChildIDS;
            serilizableObject.dialogue_id = tempArr[index].DialogueID;
            jsonObject.Add(serilizableObject);
        }

        foreach (var item in jsonObject)
        {
            Debug.Log(item.dialogue_id.ToString() + " --- " + item.map.ToString() +
                      item.npc.ToString() + " --- " + item.quest_achieved.ToString() +
                      item.parent_id.ToString() + " --- " + item.dialogue_text.ToString()
                      + " --- " + item.child_dialogues.ToString());
        }
        return jsonObject;
    }

    //Alternatif bir serilize yöntemi deprecate edildi.
    //public List<SerilizationObject> WriteFile(Dictionary<short, short> pRelationTree,
    //                                          short parent_id_pass,
    //                                          bool quest_achieved_pass,
    //                                          Quest.EQuestGiver npc_pass,
    //                                          Map map_pass,
    //                                          string dialogue_text_pass,
    //                                          short[] child_dialogues_pass,
    //                                          short dialogue_id_pass)
    //{
    //    if (Utility.IsNull<List<SerilizationObject>>(jsonObject))
    //    {
    //        Utility.CreateObjectSerially<List<SerilizationObject>>(ref jsonObject);
    //    }

    //    SerilizationObject serilizableObject = new SerilizationObject();

    //    Dialogue[] tempArr = Dialogue.LDialogues.ToArray();
    //    serilizableObject.parent_id = parent_id_pass;
    //    serilizableObject.quest_achieved = quest_achieved_pass;
    //    serilizableObject.npc = npc_pass;
    //    serilizableObject.map = map_pass;
    //    serilizableObject.dialogue_text = dialogue_text_pass;
    //    serilizableObject.child_dialogues = child_dialogues_pass;
    //    serilizableObject.dialogue_id = dialogue_id_pass;
    //    jsonObject.Add(serilizableObject);


    //    foreach (var item in jsonObject)
    //    {
    //        Debug.Log(item.dialogue_id.ToString() + " --- " + item.map.ToString() +
    //                  item.npc.ToString() + " --- " + item.quest_achieved.ToString() +
    //                  item.parent_id.ToString() + " --- " + item.dialogue_text.ToString()
    //                  + " --- " + item.child_dialogues.ToString());
    //    }
    //    return jsonObject;
    //}

}
/*
    {
        "parentName(ROOT)" :
            {
                "subParents(CHILD)" : 
                [
                    {
                        "child1Name" : "name"
                        another infs
                    },
                    {
                        "child2Name" : "name"
                        another infs
                    },
                    ..
                    ..
                    ..
                ]
            }
    } 
*/

public struct SerilizationObject
{
    public string dialogue_text;
    public Quest.EQuestGiver npc;
    public short dialogue_id;
    public short parent_id;
    public Map map;
    public bool quest_achieved;
    public short[] child_dialogues;
}