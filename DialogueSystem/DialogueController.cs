﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General; 

namespace DialogueSys
{
    public class DialogueController
    {
        static public List<Dictionary<short, short>> LDialogueRelationalTrees;
        static public Dictionary<short,Map> DDialogueMap;
        static public Dictionary<short, short> DDialogueRelational;
        static public Dictionary<short, Quest.EQuestGiver> DQuestNPC;
        static private DialogueController m_Instance; 

        // Singleton for memory saves
        static public DialogueController Instance
        {
            get
            {
                Utility.CreateObjectSerially<DialogueController>(ref m_Instance);
                
                if (!Utility.IsNull<DialogueController>(m_Instance))
                {
                    if (Utility.IsNull<List<Dialogue>>(DialogueList.AllDialogues) ||
                        Utility.IsNull<Dictionary<short, short>>(DDialogueRelational) ||
                        Utility.IsNull<List<Dictionary<short, short>>>(LDialogueRelationalTrees) ||
                        Utility.IsNull<Dictionary<short,Map>>(DDialogueMap) ||
                        Utility.IsNull<Dictionary<short , Quest.EQuestGiver>>(DQuestNPC))
                    {
                        Utility.CreateObjectSerially<Dictionary<short, short>>(ref DDialogueRelational);
                        Utility.CreateObjectSerially<List<Dictionary<short, short>>>(ref LDialogueRelationalTrees);
                        Utility.CreateObjectSerially<Dictionary<short,Map>>(ref DDialogueMap);
                        Utility.CreateObjectSerially<Dictionary<short, Quest.EQuestGiver>>(ref DQuestNPC);
                    }
                }
                else
                {
                    Debug.Log("Use this function after the creation instance of DialogueController!");
                }  
                return m_Instance;
            }
        }
         
        public void MakeRelationTree(List<Dialogue> pDialogues)
        {
            DDialogueRelational.Clear();
            DDialogueMap.Clear();
            DQuestNPC.Clear();

            if(General.Utility.IsNull<List<Dialogue>>(pDialogues))
            {
                General.Utility.CreateObjectSerially<List<Dialogue>>(ref pDialogues);
            } 

            foreach (var dialogue in pDialogues)
            {
                if (!DDialogueRelational.ContainsKey(dialogue.DialogueID))
                {
                    DDialogueRelational.Add(dialogue.DialogueID, dialogue.ParentID);
                    DDialogueMap.Add(dialogue.DialogueID, dialogue.MapType);
                    DQuestNPC.Add(dialogue.DialogueID, dialogue.QuestGiver);
                    Debug.Log(dialogue.ToString() + " DialogueID ---> " + dialogue.DialogueID.ToString() + "--> Parent ID : " + dialogue.ParentID);
                    Debug.Log("Haritasında = " + dialogue.MapType.ToString() + " -- Diyalog Sahibi NPC = " + dialogue.QuestGiver.ToString());
                }
            }
        }
    }
} 

// Bu alan uzayı tekrarlanan işleri hızlandıracak fonksiyon ve sınıflar içerecek
namespace General
{
    public class Utility
    {
        static public bool IsNull<T>(T val)
        {
            if(val == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static public bool IfItIsNullCreate<T>(ref T val) where T : new() 
        { 
            if(IsNull<T>(val))
            {
                CreateObjectSerially<T>(ref val);
                return true;
            }
            else
            {
                return false;
            }
        }
        static public void IfItIsNullCreateNonReliable<T>(ref T val) where T : new() 
        { 
            if(IsNull<T>(val))
            {
                CreateObjectSerially<T>(ref val); 
            } 
        }
        
        static public void CreateObjectSerially<T>(ref T val) where T : new()
        { 
            if (val == null)
            {
                val = new T(); 
            }
            else
            {
                Debug.Log("Seri şekilde obje oluşturulamadı");
            }
        }

        static public void Debugger<T>(T val , string context)
        {
            if(typeof(T) != typeof(string))
            {
                string text = context + " -- " + val.ToString();
                Debug.Log(text);
            }
        } 

        static public void DebuggerText<T>(T val, string context , UnityEngine.UI.Text ui_text)
        {
            if (typeof(T) != typeof(string))
            {
                string text = context + " -- " + val.ToString();
                Debug.Log(text);
                ui_text.text = text;
            }
            else
            {
                string text = context + val;
                Debug.Log(text);
                ui_text.text = text;
            }
        }

    }
}
