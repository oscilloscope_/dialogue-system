﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Quest;

// TODO : Quest sistemine göre ekleme veya çıkarma yapmamız gerekecek 
[CreateAssetMenu(fileName = "dialogue" , menuName = "Create Dialogue")]
public class Dialogue : ScriptableObject
{   
    private Dialogue()
    {
        if (this.DialogueID == 0)
        {
            ParentID = -1;
        }

        DialogueList.IDCount++;
    }

    ~Dialogue()
    {
        DialogueList.IDCount--;
    }

    /// <summary>
    /// Diyalog metni buraya yazılacak
    /// </summary>
    [Multiline]
    public string DialogueText; 

    /// <summary>
    /// Diyalog ağacındaki ilişkiler için her diyalog objesinin bağlı 
    /// olduğu diyaloğun idsini vermek gerekiyor
    /// </summary> 
    [Tooltip("Dialogue IDsi 0 olan en üsteki diyalogtur.(root)")]
    public short ParentID; // –32,768 to 32,767 shor type

    /// <summary>
    /// Diyalog ağacındaki ilişkide kendisine bağlı olan diyalogların idsi.
    /// </summary>
    [Tooltip("Kendisine bağlı diyaloğun idsi")]
    public short[] ChildIDS;

    /// <summary>
    /// Diyaloğun sahibi olacak NPC
    /// </summary>
    [Tooltip("Diyaloğun sahibi NPC")]
    public EQuestGiver QuestGiver;
    
    /// <summary>
    /// Diyaloğun geçeceği haritayı diyalogların ilişkisini
    /// belirlerken ve özellik jSon dosyasıan serilize ederken
    /// kullanacağız.
    /// </summary>
    [Tooltip("Diyaloğun geçeceği map")]
    public Map MapType;

    /// <summary>
    /// Editördeki SOler için gerekli sadece okunabilir id
    /// </summary> 
    public short DialogueID = DialogueList.IDCount;

    /// <summary>
    /// Diyaloğun NPC'de çıkıp çıkmayacağını belirliyor.Eğer bir göreve bağlıysa diyalog
    /// görev tamamlandığında NPC diyaloğuna yansıyacak
    /// </summary>
    public bool Achieved;


} 
 

public enum Map
{
    map1,
    map2,
    map3
}
 
